package net.javajoy.jps.homework.w04; //AN: имя пакета лучше в един. числе делать

/**
 * The interface defines two methods work on a two-dimensional matrix.
 *
 * @author Barkenov B.
 */

public interface IMatrix {
    IMatrix multiply(IMatrix matrix);

    IMatrix add(IMatrix matrix);

    int[][] getData();
}
