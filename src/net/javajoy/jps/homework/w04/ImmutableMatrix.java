package net.javajoy.jps.homework.w04;

/**
 * Immutable class implements the interface methods
 *
 * @author Barkenov B.
 */
//AN: класс не несет никакой логики, просто перевызов методов.
//AN: почему не клонируешь массив или не создаешь новый объект ImmutableMatrix.
//AN: Сейчас я мого через getData() получить ссылку на исходный массив и менять его, хотя Immutable класс такое не должен позволять

//AN: Знаю что вначале этот паттерн тяжело понять в ООП. Попробуй еще о нем прочитать. Смысл в том что объектные поля этого класса остаются с исходными значениями всегда
//AN: и мы никогда не даем на них прямые ссылки, так как внешний пользователь сможет его поменять и тогда все сломается.

// BB: раз мы наследуемся от Mutable, то мы получаем от него методы, но это по сути другой объект, который должен при создании
// BB: быть защищенным от изменений. СОответственно если я складываю два немутабельных объекта, я должен создать третий объект?
public final class ImmutableMatrix extends MutableMatrix {

    private int[][] matrix;

    public ImmutableMatrix(int[][] arr) {
        this.matrix = arr;
    }

    public ImmutableMatrix(int dim1, int dim2, int minValue, int maxValue) {
        super(dim1, dim2, minValue, maxValue);
    }

    public ImmutableMatrix() {
        super();
    }

    /**
     * Method produces <i>addition</i> to the the original array another array
     *
     * @param matrix array which must be multiplied
     * @return the result of adding, or the original array
     */
    @Override
    public IMatrix add(IMatrix matrix) {
        return super.add(matrix);
    }

    /**
     * Method produces <i>multiplying</i> to the the original array another array
     *
     * @param matrix array which must be multiplied
     * @return the result of multiplying, or the original array
     */
    @Override
    public IMatrix multiply(IMatrix matrix) {
        return super.multiply(matrix);
    }

    /**
     * Prints an array as regular matrix view
     *
     * @return a string representation of the array
     */
    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public int[][] getData() {
        return super.getData();
    }
}
