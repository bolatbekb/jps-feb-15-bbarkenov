package net.javajoy.jps.homework.w04;

import java.util.Random;

/**
 * Mutable class implements the interface methods
 *
 * @author Barkenov B.
 */
public class MutableMatrix implements IMatrix {

    private int[][] matrix;

    public MutableMatrix(int[][] arr) {
        if (isArrValid(arr)) this.matrix = arr;
        else this.matrix = null;
    }

    /**
     * Constructor creates an array with dimension [dim1][dim2] with randomly values
     *
     * @param dim1     the number of rows
     * @param dim2     the number of columns
     * @param minValue minimal value in the array
     * @param maxValue maximal value in the array
     */
    public MutableMatrix(int dim1, int dim2, int minValue, int maxValue) {
        if (!isArrValid(dim1, dim2)) this.matrix = null;
        else {
            Random rand = new Random();
            matrix = new int[dim1][dim2];

            for (int i = 0; i < dim1; i++) {
                for (int j = 0; j < dim2; j++) {
                    this.matrix[i][j] = rand.nextInt(maxValue - minValue + 1) + minValue;
                }
            }
        }
    }

    public MutableMatrix() {
    }

    private boolean isArrValid(int[][] arr) {
        int arrLength = arr[0].length;
        for (int i = 0; i < arr.length; i++) {
            if (arrLength != arr[i].length) {
                return false;
            }
        }
        return true;
    }

    private boolean isArrValid(int dim1, int dim2) {
        return (dim1 < 1) || (dim2 < 1) ? false : true;
    }

    /**
     * Method produces <i>addition</i> to the the original array another array
     *
     * @param matrix array which must be multiplied
     * @return the result of adding, or the original array
     */
    @Override
    public IMatrix add(IMatrix matrix) {
        int[][] mx1 = this.matrix;
        int[][] mx2 = matrix.getData();

        if (!canBeAdded(mx1, mx2)) {
            System.out.println("Не удалось произвести операцию сложения массивов - не совпадают размерности массивов\n" +
                    "Проверьте исходные данные.\nНа печать будет выведен исходный массив.\n");
            //AN: вывод сообщений лучше не делать в классах компонентах или сущностях
            //BB: Саму ошибку выводить в DemoProgramm04? Как правильно? По стилю 1С - я бы ошибку показал в классе MutableMatrix
            //BB: потому что я пытаюсь воспользоваться его методами и значит проверяю в нем же валидность массивов.
            //BB: или вернул бы в DemoProgramm04 после вызова текущего метода некий код ошибки, а потом искал бы в массиве ошибок нужный текст сообщения
            return this;
        }

        for (int i = 0; i < mx1.length; i++) {
            for (int j = 0; j < mx1[0].length; j++) {
                mx1[i][j] += mx2[i][j];
            }
        }
        this.matrix = mx1;
        return this;
    }

    /**
     * Method produces <i>multiplying</i> to the the original array another array
     *
     * @param matrix array which must be multiplied
     * @return the result of multiplying, or the original array
     */
    @Override
    public IMatrix multiply(IMatrix matrix) {
        int[][] mx1 = this.matrix;
        int[][] mx2 = matrix.getData();

        if (!canBeMultiplied(mx1, mx2)) {
            System.out.println("Не удалось произвести операцию умножения матриц - не совпадают размерности массивов\n" +
                    "Проверьте исходные данные.\nНа печать будет выведен исходный массив.\n");
            return this;
        }

        int[][] result = new int[mx1.length][mx2[0].length];
        int sum;
        for (int i = 0; i < mx1.length; i++) {
            for (int j = 0; j < mx2[0].length; j++) {
                sum = 0;
                for (int k = 0; k < mx2.length; k++) {
                    sum += mx1[i][k] * mx2[k][j];
                }
                result[i][j] = sum;
            }
        }
        this.matrix = result;
        return this;
    }

    /**
     * Checks whether 'multiply' operation in possible based on two operand arrays
     *
     * @param mx1 first array
     * @param mx2 second array
     * @return boolean returns the result of the check to the multiplying
     */
    private boolean canBeMultiplied(int[][] mx1, int[][] mx2) {
        return mx1[0].length == mx2.length;
    }

    /**
     * Checks whether 'add' operation in possible based on two operand arrays
     *
     * @param mx1 first array
     * @param mx2 second array
     * @return returns the result of the check to the adding
     */
    private boolean canBeAdded(int[][] mx1, int[][] mx2) {
        return mx1.length == mx2.length && mx1[0].length == mx2[0].length;
    }

    /**
     * Prints an array as regular matrix view
     *
     * @return a string representation of the array
     */
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int[] aMatrix : matrix) {
            for (int j = 0; j < aMatrix.length; j++) {
                result.append(aMatrix[j]).append((aMatrix.length - 1) == j ? "" : " | ");
            }
            result.append("\n");
        }
        return result.toString();
    }

    @Override
    public int[][] getData() {
        return this.matrix;
    }
}