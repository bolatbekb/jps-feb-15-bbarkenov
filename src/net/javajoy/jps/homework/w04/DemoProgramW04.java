package net.javajoy.jps.homework.w04;

/**
 * <p>These class implements the creation of two matrices based on two (mutable and immutable) classes and produces
 * over them manipulation of addition and multiplication.</p>
 * <p>Matrices are generated randomly within the constraints of the size of the array, and the values in the array.</p>
 *
 * @author Barkenov B.
 */
//AN: в целом все хорошо. Код хорошо читается. Немного нужно усовершенстововать функционал и сделать ImmutableMatrix
public class DemoProgramW04 {

    public static void main(String[] args) {
        //System.out.println("Пример создания mutable класса...");
        //demoMutableMatrix();

//        System.out.println("******************************************");
//
//        System.out.println("Пример создания immutable класса...");
        demoImmutableMatrix();
    }

    public static void demoMutableMatrix() {
        //AN: проводить типы нужно к более абстрактному
        IMatrix matrix1 = new MutableMatrix(10, 10, 0, 10);
        IMatrix matrix2 = new MutableMatrix(10, 10, -100, 0);
        // AN: неудобный конструктор. Мне нужно думать чтобы задать одинаковую размероность здесь 10 10
        // AN: и здесь 10 10, чтобы сложение отработало
        // BB: ну, обычный конструктор, к нему будет описание параметров, что это размеры ))) и как их задавать.
        // BB: Если программист ошибется при использовании моего класса - я это сообщу при попытке произвести над ними действия

        System.out.println("Массив 1:\n" + matrix1.toString());
        System.out.println("------------------------------");
        System.out.println("Массив 2:\n" + matrix2.toString());

        System.out.println("------ Результат сложения матриц 1 и 2 ------");
        matrix1.add(matrix2);
        System.out.println(matrix1.toString());

        System.out.println("=======================================");

        IMatrix matrix3 = new MutableMatrix(10, 4, 0, 10);
        IMatrix matrix4 = new MutableMatrix(4, 12, -5, 25);

        System.out.println("Массив 3:\n" + matrix3.toString());
        System.out.println("------------------------------");
        System.out.println("Массив 4:\n" + matrix4.toString());

        System.out.println("------ Результат умножения матриц 3 и 4 --------");
        matrix3.multiply(matrix4);
        System.out.println(matrix3.toString());
    }

    private static void demoImmutableMatrix() {
//        IMatrix matrix1 = new ImmutableMatrix(10, 10, 0, 10);
//        IMatrix matrix2 = new ImmutableMatrix(10, 10, -5, 25);
//
//        System.out.println("Массив 1:\n" + matrix1.toString());
//        System.out.println("------------------------------");
//        System.out.println("Массив 2:\n" + matrix2.toString());
//
//        System.out.println("------ Результат сложения матриц 1 и 2 ------");
//        matrix1.add(matrix2);
//        System.out.println(matrix1.toString());
//
//        System.out.println("=======================================");
//
//        IMatrix matrix3 = new ImmutableMatrix(10, 3, 0, 10);
//        IMatrix matrix4 = new ImmutableMatrix(3, 10, -5, 25);
//
//        System.out.println("Массив 3:\n" + matrix3.toString());
//        System.out.println("------------------------------");
//        System.out.println("Массив 4:\n" + matrix4.toString());
//
//        System.out.println("------ Результат умножения матриц 3 и 4 --------");
//        matrix3.multiply(matrix4);
//        System.out.println(matrix3.toString());
    }
}
