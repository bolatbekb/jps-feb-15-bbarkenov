package net.javajoy.jps.homework.w05;

public class DemoProgramW05 {
    public static void main(String[] args) {
        String sArray[] = {
                "Three Laws of Robotics:",
                "1. A robot may not injure a human being or, through inaction, allow a human being to come to harm.",
                "2. A robot must obey orders given it by human beings except where such orders would conflict with the First Law.",
                "3. A robot must protect its own existence as long as such protection does not conflict with the First or Second Law.",
        };

        demoPrintByIterator(sArray); // ��� �������, ������ ������ �� �������


        int stringLength = 50;
        demoCheckOfStringLength(sArray, stringLength);
    }

    private static void demoCheckOfStringLength(String[] sArray, int sl) {
        MyArray myArray = new MyArray(sArray);
        MyArray.Iterator iterator = myArray.new Iterator(myArray);
        while (iterator.hasNext()) {
            System.out.println(sl - iterator.next().length());
        }

    }

    private static void demoPrintByIterator(String[] sArray) {
        MyArray myArray = new MyArray(sArray);
        MyArray.Iterator iterator = myArray.new Iterator(myArray);
        while (iterator.hasNext()) {
            System.out.println(iterator.next().toString());
        }
    }
}
