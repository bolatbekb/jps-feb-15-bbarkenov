package net.javajoy.jps.homework.w05;

public class MyArray {
    private String[] array;

    public MyArray(String[] array) {
        this.array = array;
    }

    public class Iterator {

        private MyArray _arr;
        int currentItem;

        public Iterator(MyArray array) {
            _arr = array;
            currentItem = 0;
        }

        public boolean hasNext() {
            return currentItem < _arr.array.length;
        }

        public String next() {
            String nextItem;
            nextItem = _arr.array[currentItem];
            currentItem++;
            return nextItem;
        }
    }

    public String toString() {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < this.array.length; i++) {
            s = s.append(this.array[i]);
            if (i < this.array.length) s = s.append("\n");
        }
        return s.toString();
    }
}
