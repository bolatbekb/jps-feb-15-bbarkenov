package net.javajoy.jps.homework.w00;

/**
 * Created by barkenov.b on 16.03.2015.
 */
public class DemoNotUse {
    public static void main(String[] args) {
//        BigInteger a = valueOf(parseLong("123456781111111111"));
//        BigInteger b = valueOf(parseLong("234567891111111111"));
//        BigInteger c = valueOf(parseLong("345678901111111111"));
//        BigInteger d = valueOf(parseLong("0"));
//        long timeout = System.currentTimeMillis();
//        System.out.println(System.currentTimeMillis() - timeout);
//
//        for (int i = 0; i < 10000000; i++) {
//            d = ((a.multiply(b)).divide(c)).add(a).subtract(b);
//        }
//
//        System.out.println(System.currentTimeMillis() - timeout);

        double a = 123456781111111111D;
        double b = 234567891111111111D;
        double c = 345678901111111111D;
        double d = 0D;

        long timeout = System.currentTimeMillis();
        System.out.println(System.currentTimeMillis() - timeout);

        for (int i = 0; i < 10000000; i++) {
            d = a * b / c + a - b;
        }

        System.out.println(System.currentTimeMillis() - timeout);
        System.out.println(d);

    }
}
