package net.javajoy.jps.homework.w01;

public class Homework {

    public static void main(String[] args) {

        int sum = 0;
        int j = 0;

        int range = 100;
        int intermediateSumIndex = 10;
        int[] array = new int[range / intermediateSumIndex];

        for (int i = 0; i < range; i++) {
            sum = sum + i + 1;

            if ((i + 1) % intermediateSumIndex == 0) {
                array[j] = sum;
                j++;
                System.out.println("Сумма элементов с 1 по " + (i + 1) + " = " + sum);
            }
        }

        System.out.println("-------------------------\nОбщая сумма равна: " + sum);

        boolean sorted;

        for (int i = array.length - 1; i >= 1; i--) {
            sorted = true;

            for (int k = 0; k < i; k++) {
                if (array[k] < array[k + 1]) {
                    int temp = array[k];
                    array[k] = array[k + 1];
                    array[k + 1] = temp;
                    sorted = false;
                }
            }
            if (sorted) {
                break;
            }
        }

        System.out.println("-------------------------\nВывод отсортированного массива");
        for (int anArray : array) {
            System.out.println(anArray);
        }
    }
}
