package net.javajoy.jps.homework.w01.additional;

import java.util.Random;

//    Создайте	массив	из	12	случайных	целых	чисел	из	отрезка	[-20;20].	Определите
//    какой	элемент	является	в	этом	массиве	максимальным	и	сообщите	индекс	его
//    последнего	вхождения	в	массив.

public class Task07 {
    public static void main(String[] args) {
        int range = 12;
        int maxRandRange = 20;
        int minRandRange = -20;

        int[] array = new int[range];
        Random rand = new Random();

        for (int i = 0; i < range; i++) {
            array[i] = rand.nextInt(maxRandRange - minRandRange + 1) + minRandRange;
        }

        Utils.printArrayInLine(array);

        int maxElement = array[0];
        int maxIndex = 0;

        for (int i = 0; i < range - 1; i++) {
            if (maxElement <= array[i]) {
                maxElement = array[i];
                maxIndex = i;
            }
        }

        System.out.println("\n------------------------");
        System.out.println("Max element: " + maxElement);
        System.out.println("Max index: " + maxIndex);
    }
}
