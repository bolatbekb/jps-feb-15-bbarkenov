package net.javajoy.jps.homework.w01.additional;

import java.util.Random;

/*
    Создайте	2	массива	из	5	случайных	целых	чисел	из	отрезка	[0;5]	каждый,	выведите
    массивы	на	экран	в	двух	отдельных	строках.	Посчитайте	среднее	арифметическое
    элементов	каждого	массива	и	сообщите,	для	какого	из	массивов	это	значение
    оказалось	больше	(либо	сообщите,	что	их	средние	арифметические	равны).
*/

public class Task04 {
    public static void main(String[] args) {
        int range = 5;

        Task04 task = new Task04();

        int[] array01 = task.generateArr(range);
        int[] array02 = task.generateArr(range);

        System.out.println("Выведем массивы на экран:");
        task.printArr(array01);
        System.out.println("");
        task.printArr(array02);

        float sum01 = task.calculateSum(array01);
        float sum02 = task.calculateSum(array02);

        float average01 = sum01 / array01.length;
        float average02 = sum02 / array02.length;

        System.out.println("\nСумма первого массива: " + sum01);
        System.out.println("Среднее арифметическое первого массива: " + average01);
        System.out.println("Сумма второго массива: " + sum02);
        System.out.println("Среднее арифметическое второго массива: " + average02);

        task.compare(average01, average02);
    }

    private int[] generateArr(int range) {
        Random rand = new Random();
        int maxRandRange = 5;
        int minRandRange = 0;

        int[] myArray = new int[range];

        for (int i = 0; i < range; i++) {
            myArray[i] = rand.nextInt(maxRandRange - minRandRange + 1) + minRandRange;
        }
        return myArray;
    }

    private float calculateSum(int[] array) {
        int sum = 0;
        for (int aArray : array) {
            sum += aArray;
        }
        return sum;
    }

    private void printArr(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }

    private void compare(float a, float b) {
        if (a > b) {
            System.out.println("Среднее арифметическое первого массива больше");
        } else if (a == b) {
            System.out.println("Среднее арифметическое массивов равны");
        } else {
            System.out.println("Среднее арифметическое второго массива больше");
        }
    }
}
