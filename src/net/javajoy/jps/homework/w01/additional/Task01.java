package net.javajoy.jps.homework.w01.additional;

import static net.javajoy.jps.homework.w01.additional.Utils.printArrayWithMessage;

/*
* Создайте массив из всех нечётных чисел от 3 до 35 и выведите элементы массива на экран сначала
* в строку, отделяя один элемент от другого пробелом, а затем в столбик (отделяя один элемент от
* другого началом новой строки). Перед созданием массива подумайте, какого он будет размера.
*/
// AS: OK. Done.
public class Task01 {

    public static final int MAX_RANGE = 35;
    public static final int MIN_RANGE = 3;

    public static void main(String[] args) {
        int[] myArray = generateArr();
        printArrayWithMessage(myArray, " ", "Выводим в строку через пробел:");
        System.out.println();
        printArrayWithMessage(myArray, "\n", "Выводим в столбик:");
    }

    private static int[] generateArr() {
        int range = (MAX_RANGE - MIN_RANGE) / 2 + 1;
        int[] newArray = new int[range];
        int j = 0;

        for (int i = MIN_RANGE; i <= MAX_RANGE; i++) {
            if (i % 2 != 0) {
                newArray[j] = i;
                j++;
            }
        }
        return newArray;
    }
}
