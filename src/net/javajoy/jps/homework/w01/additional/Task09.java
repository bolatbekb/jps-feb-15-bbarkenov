package net.javajoy.jps.homework.w01.additional;

import java.util.Random;

/**
 * Created by barkenov.b on 10.03.2015.
 */

/*
    Создайте	массив	из	11	случайных	целых	чисел	из	отрезка	[-1;1],	выведите	массив
    на	экран	в	строку.	Определите	какой	элемент	встречается	в	массиве	чаще	всего	и
    выведите	об	этом	сообщение	на	экран.	Если	два	каких-то	элемента	встречаются
    одинаковое	количество	раз,	то	не	выводите	ничего.
*/

//Решение - всего разных чисел три: -1, 0, 1. Создадим три переменных и будем считать.

public class Task09 {

    public static void main(String[] args) {
        int range = 11;
        int maxRandRange = 1;
        int minRandRange = -1;
        int[] array = new int[range];
        Random rand = new Random();
        int count1 = 0;
        int count2 = 0;
        int count3 = 0;

        for (int i = 0; i < range; i++) {
            array[i] = rand.nextInt(maxRandRange - minRandRange + 1) + minRandRange;
        }
        for (int anArray : array) {
            System.out.print(anArray + " | ");
            switch (anArray) {
                case -1:
                    count1++;
                    break;
                case 0:
                    count2++;
                    break;
                case 1:
                    count3++;
                    break;
            }
        }

        if (count1 == count2 || count2 == count3 || count3 == count1) {
            System.out.println("Не выводим ничего))");
        } else if (count1 > count2 && count1 > count3) {
            System.out.println("\nЧаще всего встречается число -1");
        } else if (count2 > count1 && count2 > count3) {
            System.out.println("\nЧаще всего встречается число 0");
        } else if (count3 > count1 && count3 > count2) {
            System.out.println("\nЧаще всего встречается число 1");
        }
    }
}
