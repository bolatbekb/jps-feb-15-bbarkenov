package net.javajoy.jps.homework.w01.additional;

public abstract class Utils {

    public static void printArrayInLine(int[] array) {
        printArrayWithMessage(array, " | ", null);
    }

    public static void printArrayInColumn(int[] array) {
        printArrayWithMessage(array, "\n", null);
    }

    public static void printArrayWithMessage(int[] tArray, String separator, String message) {
        if (message != null) {
            System.out.println(message);
        }
        for (int anArray : tArray) {
            System.out.print(anArray + separator);
        }
    }

}
