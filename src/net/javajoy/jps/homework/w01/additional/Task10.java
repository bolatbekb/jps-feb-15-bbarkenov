package net.javajoy.jps.homework.w01.additional;

import java.util.Random;
import java.util.Scanner;

/**
 * Created by barkenov.b on 10.03.2015.
 */

/*
        Пользователь	должен	указать	с	клавиатуры	чётное	положительное	число,	а
        программа	должна	создать	массив	указанного	размера	из	случайных	целых	чисел
        из	[-5;5]	и	вывести	его	на	экран	в	строку.	После	этого	программа	должна
        определить	и	сообщить	пользователю	о	том,	сумма	модулей	какой	половины
        массива	больше:	левой	или	правой,	либо	сообщить,	что	эти	суммы	модулей	равны.
        Если	пользователь	введёт	неподходящее	число,	то	программа	должна	требовать
        повторного	ввода	до	тех	пор,	пока	не	будет	указано	корректное	значение.
        Ввод	с	клавиатуры	можно	заменить	передачей	параметра	в	метод.
*/

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random rand = new Random();

        int range;
        int leftSide, rightSide;
        leftSide = 0;
        rightSide = 0;

        System.out.println("Введите четное положительное число...\n");

        if (scanner.hasNextInt()) {
            do {
                range = scanner.nextInt();
                if (range % 2 != 0 || range < 1) {
                    System.out.println("Введите повторно число");
                }
            }
            while (range % 2 != 0 || range < 1); // пока число не положительное и не четное  - повторяется цикл

            int[] array = new int[range];

            for (int i = 0; i < range; i++) {
                array[i] = rand.nextInt(11) - 5;
                if (i <= (range - 1) / 2) {
                    leftSide += Math.abs(array[i]); // считаем левую сторону
                }
                if (i > (range - 1) / 2) {
                    rightSide += Math.abs(array[i]); // считаем правую сторону
                }
            }
            // выведем массив
            for (int i = 0; i < range; i++) {
                System.out.println(array[i]);
            }
            if (leftSide > rightSide) {
                System.out.println("Сумма модулей в левой части больше: " + leftSide + " > " + rightSide);
            }
            if (leftSide < rightSide) {
                System.out.println("Сумма модулей в правой части больше: " + leftSide + " < " + rightSide);
            }
            if (leftSide == rightSide) {
                System.out.println("Сумма модулей в обеих частях одинаковы: " + leftSide + " == " + rightSide);
            }

        } else {
            System.out.println("Введено не число.");
        }


    }
}
