package net.javajoy.jps.homework.w01.additional;

import java.util.Random;

/*
    Создайте	два	массива	из	10	целых	случайных	чисел	из	отрезка	[1;9]	и	третий
    массив	из	10	действительных	чисел	(с	плавающей	точкой).	Каждый	элемент	с	i-ым
    индексом	третьего	массива	должен	равняться	отношению	элемента	из	первого
    массива	с	i-ым	индексом	к	элементу	из	второго	массива	с	i-ым	индексом.	Вывести
    все	три	массива	на	экран	(каждый	на	отдельной	строке),	затем	вывести	количество
    целых	элементов	в	третьем	массиве.
*/

public class Task08 {

    public static void main(String[] args) {
        int range = 10;
        int maxRandRange = 1;
        int minRandRange = -1;

        float[] array01 = new float[range];
        float[] array02 = new float[range];
        float[] array03 = new float[range];
        Random rand = new Random();

        for (int i = 0; i < range; i++) {
            array01[i] = rand.nextInt(maxRandRange - minRandRange + 1) + minRandRange;
            array02[i] = rand.nextInt(maxRandRange - minRandRange + 1) + minRandRange;
            array03[i] = array01[i] / array02[i];
        }

        System.out.println("1ый массив");
        arrToString(array01);
        System.out.println("\n2ый массив");
        arrToString(array02);
        System.out.println("\n3ый массив");
        arrToString(array03);

        System.out.println("\nЦелые числа в 3-м массиве");
        for (float anArray : array03) {
            if (anArray == Math.round(anArray)) {
                System.out.print(anArray + "  ");
            }
        }
    }
    private static void arrToString (float[] arr){
        for (float anArray : arr) {
            System.out.print(anArray + " | ");
        }
    }
}
