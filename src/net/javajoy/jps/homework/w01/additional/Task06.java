package net.javajoy.jps.homework.w01.additional;

//Создайте	массив	из	20-ти	первых	чисел	Фибоначчи	и	выведите	его	на	экран.

public class Task06 {
    public static void main(String[] args) {
        int range = 20;

        int[] array = new int[range];

        array[0] = 0; //AN: значение по умолчанию у int и так будет 0 //BB: ok
        array[1] = 1;
        for (int i = 2; i < range; i++) {
            array[i] = array[i - 1] + array[i - 2];
        }

        System.out.println("Числа Фибоначчи до " + range);
        for (int anArray : array) {
            System.out.println(anArray);
        }
    }
}
