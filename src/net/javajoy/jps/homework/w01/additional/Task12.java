package net.javajoy.jps.homework.w01.additional;

import java.util.Random;
import java.util.Scanner;

/**
 * Created by barkenov.b on 10.03.2015.
 */

/*
        Пользователь	вводит	с	клавиатуры	натуральное	число	большее	3,	которое
        сохраняется	в	переменную	n.	Если	пользователь	ввёл	не	подходящее	число,	то
        программа	должна	просить	пользователя	повторить	ввод.	Создать	массив	из	n
        случайных	целых	чисел	из	отрезка	[0;n]	и	вывести	его	на	экран.	Создать	второй
        массив	только	из	чётных	элементов	первого	массива,	если	они	там есть,	и	вывести
        его	на	экран.
*/
public class Task12 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random rand = new Random();
        int n;

        System.out.println("Введите натуральное число, больше 3");
        //AN: слишком сложный дальше пошел для чтения код. Лучше разбить на под программы, то есть хотя бы на static методы
        if (scanner.hasNextInt()) {
            do {
                n = scanner.nextInt();
                if (n < 4) {
                    System.out.println("Введите натуральное число, больше 3");
                }
            }
            while (n < 4);

            int[] array = new int[n];
            int count = 0;
            for (int i = 0; i < n; i++) {
                array[i] = rand.nextInt(n + 1);
                if (array[i] % 2 == 0) {
                    count++;
                }
            }

            System.out.println("Вывод заполненного первого массива.");
            for (int i = 0; i < n; i++) {
                System.out.println(array[i]);
            } //AN: распечать массив можно через Arrays.toString(array);

            if (count > 0) {
                int[] arraySecond = new int[count];
                int j = 0;
                for (int i = 0; i < n; i++) {
                    if (array[i] % 2 == 0) {
                        arraySecond[j] = array[i];
                        j++;
                    }
                }
                System.out.println("Вывод второго массива - с четными числами");
                for (int anArray : arraySecond) {
                    System.out.println(anArray);
                }
            }
            if (count <= 0) {
                System.out.println("В первом массиве нет четных чисел.");
            }
        } else {
            System.out.println("Вы ввели не число.\n Запустите программу снова.");
        }
    }
}




