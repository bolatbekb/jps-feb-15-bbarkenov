package net.javajoy.jps.homework.w01.additional;

import java.util.Random;

import static net.javajoy.jps.homework.w01.additional.Utils.printArrayInColumn;

/*
* Создайте массив из 15 случайных целых чисел из отрезка [0;9]. Выведите массив на экран.
* Подсчитайте сколько в массиве чётных элементов и выведете это количество на экран на
* отдельной строке.
*/
// AS: OK. Done.
public class Task02 {
    public static final int RANGE = 15;

    public static void main(String[] args) {

        int[] myArray = new int[RANGE];
        int evenNumbers = 0;
        Random rand = new Random();

        for (int i = 0; i < RANGE; i++) {
            myArray[i] = rand.nextInt(10);
            if (myArray[i] % 2 == 0) {
                evenNumbers++;
            }
        }

        System.out.println("Выводим в столбик");
        printArrayInColumn(myArray);

        System.out.println("Всего четных элементов = " + evenNumbers);
    }
}
