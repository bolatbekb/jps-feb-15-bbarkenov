package net.javajoy.jps.homework.w01.additional;

import java.util.Random;

/**
 * Created by barkenov.b on 10.03.2015.
 */

/*
        Программа	должна	создать	массив	из	12	случайных	целых	чисел	из	отрезка	[-10;10]
        таким	образом,	чтобы	отрицательных	и	положительных	элементов	там	было
        поровну	и	не	было	нулей.	При	этом	порядок	следования	элементов	должен	быть
        случаен	(т.	е.	не	подходит	вариант,	когда	в	массиве	постоянно	выпадает	сначала	6
        положительных,	а	потом	6	отрицательных	чисел	или	же	когда	элементы	постоянно
        чередуются	через	один	и	пр.).	Вывести	полученный	массив	на	экран.
*/

public class Task11 {

    public static void main(String[] args) {
        int range = 12;
        int maxRandRange = 10;
        int minRandRange = -10;

        int countP = 0;
        int countN = 0;

        int[] array = new int[range];
        Random rand = new Random();

        do {
            for (int i = 0; i < range; i++) {
                array[i] = rand.nextInt(maxRandRange - minRandRange + 1) + minRandRange;

                if (array[i] == 0) {
                    i--;
                    continue;
                }
                if (array[i] > 0) {
                    countP++;
                }
                if (array[i] < 0) {
                    countN++;
                }
                if (i == range - 1 & countP != range / 2) {
                    countN = 0;
                    countP = 0;
                }
            }
        }
        while (countN != range / 2 & countP != range / 2);

        for (int anArray : array) {
            System.out.println(anArray);
        }
    }
}
