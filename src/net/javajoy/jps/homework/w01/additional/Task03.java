package net.javajoy.jps.homework.w01.additional;

import java.util.Random;

import static net.javajoy.jps.homework.w01.additional.Utils.printArrayInLine;

/*
 * Создайте массив из 8 случайных целых чисел из отрезка [1;10]. Выведите массив на экран в
 * строку. Замените каждый элемент с нечётным индексом на ноль. Снова выведете массив на экран
 * на отдельной строке.
 */
public class Task03 {
    public static final int RANGE = 8;
    public static final int MAX_RAND_RANGE = 10;

    public static void main(String[] args) {
        int[] myArray = new int[RANGE];

        Random rand = new Random();

        for (int i = 0; i < RANGE; i++) {
            myArray[i] = rand.nextInt(MAX_RAND_RANGE) + 1;
        }

        System.out.println("Вывод в строку:");
        printArrayInLine(myArray);

        for (int i = 0; i < RANGE; i++) {
            if (i % 2 == 1) {
                myArray[i] = 0;
            }
        }

        System.out.println("\nВывод в отдельной строке:");
        printArrayInLine(myArray);
    }


}
