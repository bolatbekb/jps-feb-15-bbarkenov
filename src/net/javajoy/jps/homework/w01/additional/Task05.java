package net.javajoy.jps.homework.w01.additional;

import java.util.Random;

/*
    Создайте	массив	из	4	случайных	целых	чисел	из	отрезка	[10;99],	выведите	его	на
    экран	в	строку.	Определить	и	вывести	на	экран	сообщение	о	том,	является	ли
    массив	строго	возрастающей	последовательностью.
*/

public class Task05 {
    public static void main(String[] args) {
        int range = 4;
        int maxRandRange = 99;
        int minRandRange = 10;
        int[] array = new int[range];
        Random rand = new Random();

        for (int i = 0; i < range; i++) {
            array[i] = rand.nextInt(maxRandRange - minRandRange + 1) + minRandRange;
        }

        for (int anArray : array) {
            System.out.println(anArray + " ");
        }

        boolean flag = true;

        for (int i = 0; i < range - 1; i++) {
            if (array[i] > array[i + 1]) {
                flag = false;
                break;
            }
        }
        System.out.println(flag ? "Массив - строго возрастающая последовательность" : "Массив - не строго возрастающая последовательность");
    }
}

