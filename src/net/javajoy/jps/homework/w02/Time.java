package net.javajoy.jps.homework.w02;

/*
    1. В классе "Время" из ДЗ 02, переопределите метод toString()
    для удобного вывода состояния объекта на печать.
 */

public class Time {

    private final static int SEC_IN_MIN = 60;
    private final static int SEC_IN_HOUR = 3600;

    private int hour;
    private int minute;
    private int second;

    public Time() {
    }

    // AS: зря переименовал, красиво было :) Но и так тоже понятно
    public Time(int hh, int mm, int ss) {
        this.hour = hh;
        this.minute = mm;
        this.second = ss;
    }

    public Time(Time time) {
        this.hour = time.hour;
        this.minute = time.minute;
        this.second = time.second;
    }

    public static Time createTime(int hh, int mm, int ss) {
        return new Time(hh, mm, ss);
    }

    public static int compareTimes(Time time1, Time time2) {
        int timeInSeconds01 = timeToSeconds(time1);
        int timeInSeconds02 = timeToSeconds(time2);

        return ((timeInSeconds01 < timeInSeconds02) ? -1 : ((timeInSeconds01 == timeInSeconds02) ? 0 : 1));
        /*
        * я еще один способ придумал если нужно возвращать именно -1/0/1 )) :
        * int difference = timeInSeconds1 - timeInSeconds2;
        * return difference != 0 ? difference/difference : 0;
        * */
    }

    public static int diffSeconds(Time time1, Time time2) {
        return Math.abs(timeToSeconds(time1) - timeToSeconds(time2));
    }

    private static int timeToSeconds(Time time) {
        return time.hour * SEC_IN_HOUR + time.minute * SEC_IN_MIN + time.second;
    }

    @Override
    public String toString() {
        String tHour = String.valueOf(this.hour);
        String tMinute = String.valueOf(this.minute);
        String tSecond = String.valueOf(this.second);

        // AS: я бы вынес дублирование в метод эту нормализацию значения, назвав, например,
        // private String normalizeValue(String timePartValue) {}, поток
        // tHour = normalizeValue(tHour);
        // либо без промежуточных переменных, а компоновал сразу выражение в return
        if (tHour.length() < 2) tHour = "0" + tHour;
        if (tMinute.length() < 2) tMinute = "0" + tMinute;
        if (tSecond.length() < 2) tSecond = "0" + tSecond;

        return tHour + ":" + tMinute + ":" + tSecond;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }
}
