package net.javajoy.jps.homework.w02;

import java.util.Scanner;

import static net.javajoy.jps.homework.w02.Time.createTime;

/*
Напишите класс «Время» для представления времени в пределах суток. Внутреннее представление  -
три целые числа, задающие часы, минуты и секунды.  Класс должен содержать:
-  конструкторы: a) по умолчанию; b) с параметрами; c) копирующий;
-  статический метод создания объекта по трем заданным числам;
-  статический метод сравнения на больше-меньше;
-  нестатический метод нахождения секунд, разделяющих два времени.

2. Создать два объекта типа «Время», заполнив их значениями из консоли, используя разные
способы создания (конструкторы) экземпляра объекта. Вывести результаты сравнения двух
объектов на консоль (какой больше из них, разница в секундах). Подумайте, как бы вы могли
обезопасить свой класс от ввода некорректных данных, например выход за диапазон допустимых значений.
Если такое происходит, то следует информировать об этом пользователя и предложить ввести тот же параметр еще раз.

3. Добавить проверку на ввод некорректных данных, таких как дробные числа или текст.
*/

// AS: все же нет примера заполнения из консоли. Есть трудности с этим?
// AS: И пункт 3 если сможешь реализовать будет отлично.
// Если есть вопросы, задавай, подскажем
// BB: заполнение из консоли - метод getTimeFromUser()
// BB: там же и проверка п.3

public class DemoProgramW02 {

    public static void main(String[] args) {
        int hour = 23;
        int min = 30;
        int sec = 45;

        Time timeDefault = new Time();
        System.out.println("timeDefault:");
        System.out.println(timeDefault.toString());

        Time time = new Time(hour, min, sec);
        System.out.println("time:");
        System.out.println(time.toString());

        Time timeCopy = new Time(time);
        System.out.println("timeCopy:");
        System.out.println(timeCopy.toString());

        Time timeStatic = createTime(hour, min, sec);
        System.out.println("timeStatic:");
        System.out.println(timeStatic.toString());

        time.setHour(20);
        time.setMinute(20);
        time.setSecond(30);

        System.out.println("time created by setters:");
        System.out.println(time.toString());

        System.out.println("-----------------------------");

        Time time01 = new Time(10, 10, 10);
        Time time02 = new Time(10, 10, 20);
        System.out.println("time01:");
        System.out.println(time01.toString());

        System.out.println("time02:");
        System.out.println(time02.toString());

        switch (Time.compareTimes(time01, time02)) {
            case 1:
                System.out.println("Первое время больше второго.");
                break;
            case 0:
                System.out.println("Оба времени одинаковы.");
                break;
            case -1:
                System.out.println("Второе время больше первого.");
                break;
        }
        System.out.printf("Разница в секундах: %s (сек).\n", Time.diffSeconds(time01, time02));

        String[] timeArray03 = getTimeFromUser("time03");
        String[] timeArray04 = getTimeFromUser("time04");
        Time time03 = new Time(toInt(timeArray03[0]), toInt(timeArray03[1]), toInt(timeArray03[2]));
        System.out.print("Time03 = ");
        System.out.println(time03.toString());

        Time time04 = createTime(toInt(timeArray04[0]), toInt(timeArray04[1]), toInt(timeArray04[2]));
        System.out.print("Time04 = ");
        System.out.println(time04.toString());

        Time.compareTimes(time03, time04);
        System.out.printf("Разница в секундах: %s (сек).\n", Time.diffSeconds(time03, time04));
    }

    private static String[] getTimeFromUser(String typeOfTime) {

        System.out.println("-----------------------------");
        System.out.printf("Введите время '%s' в формате hh:mm:ss\n", typeOfTime);

        String[] timeArray = new String[3];
        String pattern = "^(([0,1][0-9])|(2[0-3])):[0-5][0-9]:[0-5][0-9]$";

        boolean x = false;

        while (!x) {
            Scanner scanner = new Scanner(System.in);
            if (scanner.hasNext(pattern)) {
                String timeText = scanner.next();
                timeArray = timeText.split(":");
                x = true;
            } else {
                System.out.printf("Вы неправильно ввели время для '%s'.\nВведите еще раз в формате hh:mm:ss.\n", typeOfTime);
            }
        }
        return timeArray;
    }

    private static int toInt(String src) {
        return Integer.parseInt(src);
    }
}
