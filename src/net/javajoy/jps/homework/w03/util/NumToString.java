package net.javajoy.jps.homework.w03.util;

public final class NumToString {
    private final static int MAX_NUMBER_OF_TRIADS = 6;

    private final static int DIGIT_POS_FOR_ONE = 1;
    private final static int DIGIT_POS_FOR_FOUR = 2;
    private final static int DIGIT_POS_FOR_MANY = 3;

    private final static int TEXT_POS_FOR_SECOND_TEN = 2;
    private final static int TEXT_POS_FOR_TENS = 3;
    private final static int TEXT_POS_FOR_HUNDREDS = 4;

    private final static String[][] ARR_DIGIT_POSITIONS = new String[][]{
            {"0", "", "", ""},
            {"1", "тысяча ", "тысячи ", "тысяч "},
            {"0", "миллион ", "миллиона ", "миллионов "},
            {"0", "миллиард ", "миллиарда ", "миллиардов "},
            {"0", "триллион ", "триллиона ", "триллионов "},
            {"0", "квадриллион ", "квадриллиона ", "квадриллионов "},
            {"0", "квинтиллион ", "квинтиллиона ", "квинтиллионов "}
    };

    private final static String[][] ARR_DIGITS_TO_STRINGS = new String[][]{
            {"", "", "десять ", "", ""},
            {"один ", "одна ", "одиннадцать ", "десять ", "сто "},
            {"два ", "две ", "двенадцать ", "двадцать ", "двести "},
            {"три ", "три ", "тринадцать ", "тридцать ", "триста "},
            {"четыре ", "четыре ", "четырнадцать ", "сорок ", "четыреста "},
            {"пять ", "пять ", "пятнадцать ", "пятьдесят ", "пятьсот "},
            {"шесть ", "шесть ", "шестнадцать ", "шестьдесят ", "шестьсот "},
            {"семь ", "семь ", "семнадцать ", "семьдесят ", "семьсот "},
            {"восемь ", "восемь ", "восемнадцать ", "восемьдесят ", "восемьсот "},
            {"девять ", "девять ", "девятнадцать ", "девяносто ", "девятьсот "}
    };

    /*
    AN: сложноватый метод. Рекомендую его разбить на части если получится
    BB: подскажите, как лучше сделать? Алгоритм был найден в сети и адаптирован для нужд задачи. Причем разобран до косточки.
    AN: мне не нравится что внтури цикла for много всего. С другой стороны если алгоритм проверенный, то его можно так и оставить.
    AN: Базовой подход к упрощению это разбиение метода на маленькие функции, то есть private методы, может быть даже со static модификатором
    BB: алгоритм проверенный
    */

    public static String toString(int num) {
        if (num == 0) {
            return "ноль ";
        }

        StringBuilder result = new StringBuilder();
        int numBuffer = num;

        if (num < 0) {
            result.append("минус ");
            numBuffer = -numBuffer;
        }

        long divisor = (long) Math.pow(1000, MAX_NUMBER_OF_TRIADS);
        int digitOrderNumber;

        for (int j = MAX_NUMBER_OF_TRIADS - 1; j >= 0; j--) {
            divisor /= 1000;
            digitOrderNumber = (int) (numBuffer / divisor);
            numBuffer %= divisor;

            if (digitOrderNumber == 0) {
                if (j > 0) continue;
                result.append(ARR_DIGIT_POSITIONS[j][DIGIT_POS_FOR_ONE]);
            } else {
                if (digitOrderNumber >= 100) {
                    result.append(ARR_DIGITS_TO_STRINGS[digitOrderNumber / 100][TEXT_POS_FOR_HUNDREDS]);
                    digitOrderNumber %= 100;
                }
                if (digitOrderNumber >= 20) {
                    result.append(ARR_DIGITS_TO_STRINGS[digitOrderNumber / 10][TEXT_POS_FOR_TENS]);
                    digitOrderNumber %= 10;
                }
                if (digitOrderNumber >= 10) {
                    result.append(ARR_DIGITS_TO_STRINGS[digitOrderNumber - 10][TEXT_POS_FOR_SECOND_TEN]);
                } else if (digitOrderNumber >= 1) {
                    result.append(ARR_DIGITS_TO_STRINGS[digitOrderNumber]["0".equals(ARR_DIGIT_POSITIONS[j][0]) ? 0 : 1]);
                }

                switch (digitOrderNumber) {
                    case 1:
                        result.append(ARR_DIGIT_POSITIONS[j][DIGIT_POS_FOR_ONE]);
                        break;
                    case 2:
                    case 3:
                    case 4:
                        result.append(ARR_DIGIT_POSITIONS[j][DIGIT_POS_FOR_FOUR]);
                        break;
                    default:
                        result.append(ARR_DIGIT_POSITIONS[j][DIGIT_POS_FOR_MANY]);
                        break;
                }
            }
        }
        return result.toString();
    }
}