package net.javajoy.jps.homework.w03;

import java.util.Random;

public class DemoProgramW03 {

    public static void main(String[] args) {
        Random rand = new Random();

        for (int i = 0; i < 10; i++) {
            DayPartTime timeExample = new DayPartTime(rand.nextInt(24), rand.nextInt(60), rand.nextInt(60));

            System.out.printf("Время: %s. Прописью: %s", formatTime(timeExample), timeExample.toString());
            System.out.printf(". Это - %s\n", timeExample.getTimeOfDay().getDayPartString());
        }
    }

    private static String formatTime(DayPartTime dayPartTime) {
        String tHour = String.valueOf(dayPartTime.getHour());
        String tMinute = String.valueOf(dayPartTime.getMinute());
        String tSecond = String.valueOf(dayPartTime.getSecond());

        return addLeadingZero(tHour) + ":" + addLeadingZero(tMinute) + ":" + addLeadingZero(tSecond);
    }

    private static String addLeadingZero(String partTime) {
        return partTime.length() < 2 ? "0" + partTime : partTime;
    }
}
