package net.javajoy.jps.homework.w03;

import net.javajoy.jps.homework.w02.Time;
import net.javajoy.jps.homework.w03.util.NumToString;

import static net.javajoy.jps.homework.w03.EDayParts.*;

public class DayPartTime extends Time {

    public DayPartTime(int hh, int mm, int ss) {
        super(hh, mm, ss);
    }

    @Override
    public String toString() {
        return String.format("%sчас(ов) : %sминут(ы) : %sсекунд(ы)",
                NumToString.toString(this.getHour()),
                NumToString.toString(this.getMinute()),
                NumToString.toString(this.getSecond()));
    }

    public EDayParts getTimeOfDay() {
        int hh = this.getHour();

        if (hh >= NIGHT.getTimeBegin().getHour() && hh < MORNING.getTimeBegin().getHour()) {
            return NIGHT;
        } else if (hh >= MORNING.getTimeBegin().getHour() && hh < AFTERNOON.getTimeBegin().getHour()) {
            return MORNING;
        } else if (hh >= AFTERNOON.getTimeBegin().getHour() && hh < EVENING.getTimeBegin().getHour()) {
            return AFTERNOON;
        } else {
            return EVENING;
        }
    }
}
