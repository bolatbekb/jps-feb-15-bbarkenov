package net.javajoy.jps.homework.w03;
//AN: лучше называть в единственном числе пакет, то есть enum
//BB:enum - зарезервированное ключевое слово, не даст переименовать. В итоге вынес выше.

public enum EDayParts {
    NIGHT(new DayPartTime(0, 0, 0), new DayPartTime(5, 59, 59), "ночь"),
    MORNING(new DayPartTime(6, 0, 0), new DayPartTime(11, 59, 59), "утро"),
    AFTERNOON(new DayPartTime(12, 0, 0), new DayPartTime(17, 59, 59), "день"),
    EVENING(new DayPartTime(18, 0, 0), new DayPartTime(23, 59, 59), "вечер");

    private final DayPartTime dayPartTimeBegin;
    private final DayPartTime dayPartTimeEnd;
    private final String dayPartString;

    EDayParts(DayPartTime timeBegin, DayPartTime timeEnd, String timeOfDayString) {
        this.dayPartTimeBegin = timeBegin;
        this.dayPartTimeEnd = timeEnd;
        this.dayPartString = timeOfDayString;
    }

    public DayPartTime getTimeBegin() {
        return dayPartTimeBegin;
    }

    public DayPartTime getTimeEnd() {
        return dayPartTimeEnd;
    }

    public String getDayPartString() {
        return dayPartString;
    }
}

